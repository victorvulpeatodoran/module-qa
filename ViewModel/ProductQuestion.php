<?php
declare(strict_types=1);

namespace Vulpea\Qa\ViewModel;

use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroupBuilder;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\DB\LoggerInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Vulpea\Qa\Api\Data\ProductQuestionInterface;
use Vulpea\Qa\Api\ProductQuestionRepositoryInterface;

/**
 * Class QuestionViewModel
 * @package Vulpea\Qa\ViewModel
 * @author Victor Todoran <victor.todoran@evozon.com>
 */
class ProductQuestion implements ArgumentInterface
{
    /**
     * @var
     */
    private $logger;
    /**
     * @var ProductQuestionRepositoryInterface
     */
    private $questionRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    public function __construct(
        LoggerInterface $logger,
        ProductQuestionRepositoryInterface $productQuestionRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder
    )
    {
        $this->logger = $logger;
        $this->questionRepository = $productQuestionRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
    }

    /**
     * @param $productId
     * @return ProductQuestionInterface[]
     */
    public function getProductQuestions($productId): array
    {
        $this->filterBuilder->setField(ProductQuestionInterface::PRODUCT_ID)
            ->setConditionType('eq')
            ->setValue($productId);
        $filter = $this->filterBuilder->create();

        $this->searchCriteriaBuilder->addFilter($filter);
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $searchResult = $this->questionRepository->getList($searchCriteria);

        return $searchResult->getItems();
    }
}