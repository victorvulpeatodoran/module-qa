<?php
declare(strict_types=1);

namespace Vulpea\Qa\ViewModel;

use Magento\Customer\Model\Customer;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\SearchCriteriaBuilderFactory;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Psr\Log\LoggerInterface;
use Vulpea\Qa\Api\Data\ProductAnswerInterface;
use Vulpea\Qa\Api\Data\ProductQuestionInterface;
use Vulpea\Qa\Api\ProductAnswerRepositoryInterface;
use Vulpea\Qa\Helper\QaConfig;
use \Vulpea\Qa\Model\ResourceModel\ProductAnswer as ProductAnswerResource;

/**
 * Class AnswerViewModel
 * @author Victor Todoran <victor.todoran@evozon.com>
 * @package Vulpea\Qa\ViewModel
 */
class ProductAnswer implements ArgumentInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ProductAnswerRepositoryInterface
     */
    private $answerRepository;

    /**
     * @var SearchCriteriaBuilderFactory
     */
    private $searchCriteriaBuilderFactory;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var ProductAnswerResource
     */
    private $productAnswerResource;

    /**
     * @var QaConfig
     */
    private $qaConfig;

    public function __construct(
        LoggerInterface $logger,
        ProductAnswerRepositoryInterface $answerRepository,
        SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory,
        ProductAnswerResource $productAnswerResource,
        FilterBuilder $filterBuilder,
        QaConfig $qaConfig
    )
    {
        $this->logger = $logger;
        $this->answerRepository = $answerRepository;
        $this->searchCriteriaBuilderFactory = $searchCriteriaBuilderFactory;
        $this->filterBuilder = $filterBuilder;
        $this->productAnswerResource = $productAnswerResource;
        $this->qaConfig = $qaConfig;
    }

    /**
     * @param $questionId
     * @return ProductAnswerInterface[]
     */
    public function getProductAnswersByQuestionId($questionId): array
    {
        /** @var \Magento\Framework\Api\Search\SearchCriteriaBuilder $searchCriteriaBuilder */
        $searchCriteriaBuilder = $this->searchCriteriaBuilderFactory->create();

        $this->filterBuilder->setField(ProductAnswerInterface::QUESTION_ID)
            ->setConditionType('eq')
            ->setValue($questionId);
        $filter = $this->filterBuilder->create();

        $searchCriteriaBuilder->addFilter($filter);
        $searchCriteria = $searchCriteriaBuilder->create();

        $searchResult = $this->answerRepository->getList($searchCriteria);

        return $searchResult->getItems();
    }

    /**
     * @param ProductQuestionInterface $productQuestion
     * @param Customer|null $loggedInCustomer
     * @return bool
     */
    public function shouldDisplayAnswerForm(ProductQuestionInterface $productQuestion, ?Customer $loggedInCustomer): bool
    {
        // guests can not answer questions.
        if(is_null($loggedInCustomer) || !$loggedInCustomer->getId()){
            return false;
        }

        // customer can not answer her own questions
        if($productQuestion->getCustomerId() == $loggedInCustomer->getId()){
            return false;
        }

        try{
            $questionHasAnswer = $this->productAnswerResource->hasAnswerByQuestionId($productQuestion->getId());
        }catch (\Exception $exception){
            $this->logger->critical($exception->getMessage(), [self::class, 'question_id' => $productQuestion->getId()]);
        }

        // only one answer per question
        if(isset($questionHasAnswer) && $questionHasAnswer){
            return false;
        }

        // only customers from configured allowed group can answer questions
        if($this->qaConfig->getAllowedCustomerGroupId() != $loggedInCustomer->getGroupId()){
            return false;
        }

        return true;
    }
}