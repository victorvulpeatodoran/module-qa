<?php
declare(strict_types=1);

namespace Vulpea\Qa\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Customer\Model\Session as CustomerSession;

/**
 * Using this class makes sense only if full_page cache is disabled.
 * @see README.md for more information about how FPC affects session data.
 *
 * Class CustomerInformation
 * @package Vulpea\Qa\ViewModel
 * @author Victor Todoran <victor.todoran@evozon.com>
 */
class CustomerInformation implements ArgumentInterface
{
    /**
     * @var CustomerSession
     */
    private $customerSession;

    public function __construct(
        CustomerSession $customerSession
    )
    {
        $this->customerSession = $customerSession;
    }

    /**
     * Only works with FPC disabled
     * @return bool
     */
    public function isCustomerLoggedIn(): bool
    {
        return $this->customerSession->isLoggedIn();
    }

    /**
     * Only works with FPC disabled
     * @return \Magento\Customer\Model\Customer
     */
    public function getLoggedCustomer()
    {
        return $this->customerSession->getCustomer();
    }
}