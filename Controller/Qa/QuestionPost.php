<?php
declare(strict_types=1);

namespace Vulpea\Qa\Controller\Qa;

use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Raw;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Model\AbstractModel;
use Psr\Log\LoggerInterface;
use Vulpea\Qa\Api\Data\ProductQuestionInterfaceFactory;
use Vulpea\Qa\Api\Data\ProductQuestionInterface;
use Vulpea\Qa\Api\ProductQuestionRepositoryInterface;
use Vulpea\Qa\Helper\QaConfig;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Message\MessageInterface;

/**
 * Class QuestionPost
 * @package Vulpea\Qa\Controller\Qa
 * @author Victor Todoran <victor.todoran@evozon.com>
 */
class QuestionPost extends Action
{
    const SUCCESS_MESSAGE = 'You successfully added a question';
    const GENERIC_FAIL_MESSAGE = 'Could not create product question';

    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var QaConfig
     */
    private $qaConfig;
    /**
     * @var ProductQuestionRepositoryInterface
     */
    private $questionRepository;
    /**
     * @var ProductQuestionInterfaceFactory
     */
    private $questionFactory;
    /**
     * @var CustomerSession
     */
    private $customerSession;
    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    private $formValidator;


    public function __construct(
        LoggerInterface $logger,
        ProductQuestionRepositoryInterface $questionRepository,
        ProductQuestionInterfaceFactory $questionFactory,
        CustomerSession $customerSession,
        QaConfig $qaConfig,
        \Magento\Framework\Data\Form\FormKey\Validator $formValidator,
        Context $context
    )
    {
        $this->logger = $logger;
        $this->qaConfig = $qaConfig;
        $this->questionRepository = $questionRepository;
        $this->questionFactory = $questionFactory;
        $this->customerSession = $customerSession;
        $this->formValidator = $formValidator;
        parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        if(!is_null($resultValidation = $this->validateRequest())){
            return $resultValidation;
        }

        $questionData = $this->getRequest()->getParams();
        $questionData[ProductQuestionInterface::CUSTOMER_ID] = $this->customerSession->getCustomerId();

        $this->saveProductQuestion($questionData);

        if($this->messageManager->getMessages()->getCountByType(MessageInterface::TYPE_ERROR) === 0){
            $this->messageManager->addSuccessMessage(self::SUCCESS_MESSAGE);
        }

        $resultRedirect = $this->getNewRedirectResultObject();
        return $resultRedirect->setUrl($this->_redirect->getRefererUrl());
    }

    /**
     * @param array $questionData
     */
    private function saveProductQuestion(array $questionData): void
    {
        /** @var ProductQuestionInterface|AbstractModel $question */
        $question = $this->questionFactory->create(['data' => $questionData]);
        $question->setHasDataChanges(true);

        try{
            $this->questionRepository->save($question);
        }catch (CouldNotSaveException $exception){
            $this->logger->debug($exception->getMessage());
            $this->messageManager->addErrorMessage(self::GENERIC_FAIL_MESSAGE);
        }

    }

    /**
     * Returns the needed result if the request is invalid
     * Returns null if request is valid - execution must continue if request is valid
     *
     * @return ResultInterface|null
     */
    private function validateRequest(): ?ResultInterface
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->getNewRedirectResultObject();

        // Form was generated. Feature was switched off. Client places request.
        if(!$this->qaConfig->isFeatureEnabled()){
            $resultRedirect->setPath('noroute');
            return $resultRedirect;
        }

        // CSRF
        if(!$this->formValidator->validate($this->getRequest())){
            /** @var Raw $resultRaw */
            $resultRaw = $this->resultFactory->create(ResultFactory::TYPE_RAW)
                ->setStatusHeader(\Magento\Framework\App\Response\Http::STATUS_CODE_403, '1.1', 'Forbidden');
            return $resultRaw;
        }

        return null;
    }

    /**
     * @return ResultInterface
     */
    private function getNewRedirectResultObject(): ResultInterface
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setRefererUrl($this->_redirect->getRefererUrl());

        return $resultRedirect;
    }
}