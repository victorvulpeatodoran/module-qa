<?php
declare(strict_types=1);

namespace Vulpea\Qa\Controller\Qa;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Raw;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Model\AbstractModel;
use Psr\Log\LoggerInterface;
use Vulpea\Qa\Api\Data\ProductAnswerInterface;
use Vulpea\Qa\Api\ProductAnswerRepositoryInterface;
use Vulpea\Qa\Helper\QaConfig;
use Magento\Customer\Model\Session;
use Vulpea\Qa\Api\Data\ProductAnswerInterfaceFactory;
use Magento\Framework\Message\MessageInterface;

/**
 * Class AnswerPost
 * @package Vulpea\Qa\Controller\Qa
 * @author Victor Todoran <victor.todoran@evozon.com>
 */
class AnswerPost extends Action
{
    const MESSAGE_NOT_ALLOWED = 'Unfortunately you are not allowed to answer this question. Contact us to know more.';
    const SUCCESS_MESSAGE = 'You successfully added an answer. Thank you for taking the time!';
    const GENERIC_FAIL_MESSAGE = 'Could not add the answer';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var QaConfig
     */
    private $qaConfig;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var ProductAnswerRepositoryInterface
     */
    private $answerRepository;

    /**
     * @var ProductAnswerInterfaceFactory
     */
    private $answerFactory;

    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    private $formValidator;

    public function __construct(
        LoggerInterface $logger,
        QaConfig $qaConfig,
        Session $session,
        ProductAnswerInterfaceFactory $answerFactory,
        ProductAnswerRepositoryInterface $answerRepository,
        \Magento\Framework\Data\Form\FormKey\Validator $formValidator,
        Context $context
    )
    {
        $this->logger = $logger;
        $this->session = $session;
        $this->qaConfig = $qaConfig;
        $this->answerRepository = $answerRepository;
        $this->answerFactory = $answerFactory;
        $this->formValidator = $formValidator;
        parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        if(!is_null($resultValidation = $this->validateRequest())){
            return $resultValidation;
        }

        $answerData = $this->getRequest()->getParams();
        $answerData[ProductAnswerInterface::CUSTOMER_ID] = $this->session->getCustomerId();

        $this->saveProductAnswer($answerData);

        if($this->messageManager->getMessages()->getCountByType(MessageInterface::TYPE_ERROR) === 0){
            $this->messageManager->addSuccessMessage(self::SUCCESS_MESSAGE);
        }

        $resultRedirect = $this->getNewRedirectResultObject();
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }

    /**
     * @param array $answerData
     */
    private function saveProductAnswer(array $answerData): void
    {
        /** @var ProductAnswerInterface| AbstractModel $answer */
        $answer = $this->answerFactory->create(['data' => $answerData]);
        $answer->setHasDataChanges(true);

        try{
            $this->answerRepository->save($answer);
        }catch (CouldNotSaveException $exception){
            $this->logger->debug($exception->getMessage());
            $this->messageManager->addErrorMessage(self::GENERIC_FAIL_MESSAGE);
        }
    }

    /**
     * Returns the needed result if the request is invalid
     * Returns null if request is valid - execution must continue if request is valid
     *
     * @return ResultInterface|null
     */
    private function validateRequest(): ?ResultInterface
    {
        $resultRedirect = $this->getNewRedirectResultObject();

        // CSRF
        if(!$this->formValidator->validate($this->getRequest())){
            /** @var Raw $resultRaw */
            return $resultRaw = $this->resultFactory->create(ResultFactory::TYPE_RAW)
                ->setStatusHeader(\Magento\Framework\App\Response\Http::STATUS_CODE_403, '1.1', 'Forbidden');
        }

        if(!$this->qaConfig->isFeatureEnabled() || !$this->session->isLoggedIn()){
            return $resultRedirect->setPath('noroute');
        }

        if(!$this->session->isLoggedIn()){
            return $resultRedirect->setPath('customer/account/login');
        }

        if($this->session->getCustomer()->getGroupId() != $this->qaConfig->getAllowedCustomerGroupId()){
            $this->messageManager->addNoticeMessage(__(self::MESSAGE_NOT_ALLOWED));
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            return $resultRedirect;
        }

        return null;
    }

    /**
     * @return ResultInterface
     */
    private function getNewRedirectResultObject(): ResultInterface
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setRefererUrl($this->_redirect->getRefererUrl());

        return $resultRedirect;
    }

}