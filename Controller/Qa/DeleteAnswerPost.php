<?php
declare(strict_types=1);

namespace Vulpea\Qa\Controller\Qa;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\DB\LoggerInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Vulpea\Qa\Api\ProductAnswerRepositoryInterface;
use Vulpea\Qa\Helper\QaConfig;
use Magento\Customer\Model\Session;

/**
 * Class DeletePost
 * @package Vulpea\Qa\Controller\Qa
 * @author Victor Todoran <victor.todoran@yahoo.com>
 */
class DeleteAnswerPost extends Action
{
    const ANSWER_ID = 'answer_id';
    const BAD_REQUEST_MESSAGE = "Invalid Request. Could not delete answer";
    const SUCCESS_MESSAGE = 'You successfully removed your answer';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var QaConfig
     */
    private $qaConfig;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var ProductAnswerRepositoryInterface
     */
    private $answerRepository;

    public function __construct(
        LoggerInterface $logger,
        QaConfig $qaConfig,
        Session $session,
        ProductAnswerRepositoryInterface $answerRepository,
        Context $context
    )
    {
        $this->logger = $logger;
        $this->session = $session;
        $this->qaConfig = $qaConfig;
        $this->answerRepository = $answerRepository;
        parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());


        if(!$this->qaConfig->isFeatureEnabled() || !$this->session->isLoggedIn()){
            // redirect to product page after login
            $resultRedirect->setRefererUrl($this->_redirect->getRefererUrl());
            $resultRedirect->setPath('noroute');
            return $resultRedirect;
        }
        // answer_id must be a valid integer
        $answerId = $this->getRequest()->getParam(self::ANSWER_ID);
        if(!$answerId || !is_numeric($answerId)){
            $this->messageManager->addErrorMessage(__(self::BAD_REQUEST_MESSAGE));
            return $resultRedirect;
        }
        $answerId = (int) $answerId;

        // TODO delete logic should not be in controller
        // there must be an answer with the provided id
        try{
            $answer = $this->answerRepository->getById($answerId);
        }catch (NoSuchEntityException $exception){
            $this->messageManager->addErrorMessage(__(self::BAD_REQUEST_MESSAGE));
            return $resultRedirect;
        }

        // a customer can only remove his own answers
        if($answer->getCustomerId() != $this->session->getCustomerId()){
            $this->messageManager->addErrorMessage(__(self::BAD_REQUEST_MESSAGE));
            return $resultRedirect;
        }

        $this->answerRepository->delete($answer);
        $this->messageManager->addSuccessMessage(__(self::SUCCESS_MESSAGE));
        return $resultRedirect;
    }

}