<?php

namespace Vulpea\Qa\Api;

use Magento\Framework\Api\Search\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Vulpea\Qa\Api\Data\ProductAnswerInterface;

/**
 * Interface ProductAnswerRepositoryInterface
 * @package Vulpea\Qa\Api
 * @author Victor Todoran <victor.todoran@evozon.com
 */
interface ProductAnswerRepositoryInterface
{
    /**
     * @param int $id
     * @return ProductAnswerInterface
     */
    public function getById(int $id): ProductAnswerInterface;

    /**
     * @param ProductAnswerInterface $question
     * @return ProductAnswerInterface
     * @throws CouldNotSaveException
     */
    public function save(ProductAnswerInterface $question): ProductAnswerInterface;

    /**
     * @param ProductAnswerInterface $question
     * @return ProductAnswerInterface
     * @throws ProductAnswerInterface
     */
    public function delete(ProductAnswerInterface $question): ProductAnswerInterface;

    /**
     * @param int $id
     * @return ProductAnswerInterface
     * @throws CouldNotDeleteException
     */
    public function deleteById(int $id): ProductAnswerInterface;

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Magento\Framework\Api\Search\SearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}