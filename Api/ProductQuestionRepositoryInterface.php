<?php

namespace Vulpea\Qa\Api;

use Magento\Framework\Api\Search\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Vulpea\Qa\Api\Data\ProductQuestionInterface;

/**
 * Interface ProductQuestionRepositoryInterface
 * @package Vulpea\Qa\Api
 * @author Victor Todoran <victor.todoran@evozon.com>
 */
interface ProductQuestionRepositoryInterface
{
    /**
     * @param int $id
     * @return ProductQuestionInterface
     */
    public function getById(int $id): ProductQuestionInterface;

    /**
     * @param ProductQuestionInterface $question
     * @return ProductQuestionInterface
     * @throws CouldNotSaveException
     */
    public function save(ProductQuestionInterface $question): ProductQuestionInterface;

    /**
     * @param ProductQuestionInterface $question
     * @return ProductQuestionInterface
     * @throws CouldNotDeleteException
     */
    public function delete(ProductQuestionInterface $question): ProductQuestionInterface;

    /**
     * @param int $id
     * @return ProductQuestionInterface
     * @throws CouldNotDeleteException
     */
    public function deleteById(int $id): ProductQuestionInterface;

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Magento\Framework\Api\Search\SearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}