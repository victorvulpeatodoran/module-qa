<?php
namespace Vulpea\Qa\Api\Data;

/**
 * Interface ProductAnswer
 * @package Vulpea\Qa\Api\Data
 * @author Victor Todoran <victor.todoran@evozon.com>
 */
interface ProductAnswerInterface
{
    const ID = 'id';
    const ANSWER = 'answer';
    const QUESTION_ID = 'question_id';
    const CUSTOMER_ID = 'customer_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * @return null|string
     */
    public function getAnswer(): ?string;

    /**
     * @return int|null
     */
    public function getQuestionId(): ?int;

    /**
     * @return int|null
     */
    public function getCustomerId(): ?int;

    /**
     * @return null|string
     */
    public function getCreatedAt(): ?string;

    /**
     * @return null|string
     */
    public function getUpdatedAt(): ?string;

}