<?php

namespace Vulpea\Qa\Api\Data;

/**
 * Interface ProductQuestionsInterface
 * @package Vulpea\Qa\Api\Data
 * @author  Victor Todoran <victor.todoran@evozon.com
 */
interface ProductQuestionInterface
{
    const ID = 'id';
    const NICKNAME = 'nickname';
    const QUESTION = 'question';
    const CUSTOMER_ID = 'customer_id';
    const PRODUCT_ID = 'product_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * @return null|string
     */
    public function getNickname(): ?string;

    /**
     * @return null|string
     */
    public function getQuestion(): ?string;

    /**
     * @return int|null
     */
    public function getCustomerId(): ?int;

    /**
     * @return int|null
     */
    public function getProductId(): ?int;
    /**
     * @return null|string
     */
    public function getCreatedAt(): ?string;

    /**
     * @return null|string
     */
    public function getUpdatedAt(): ?string;
}