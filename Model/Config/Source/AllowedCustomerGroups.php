<?php
declare(strict_types=1);

namespace Vulpea\Qa\Model\Config\Source;

use Magento\Customer\Api\Data\GroupInterface;
use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Only registered customers can answer questions
 *
 * Class AllowedCustomerGroups
 * @package Vulpea\Qa\Model\Config\Source
 */
class AllowedCustomerGroups implements OptionSourceInterface
{
    const NOT_LOGGED_IN = 'NOT LOGGED IN';

    /**
     * @var GroupRepositoryInterface
     */
    private $customerGroupRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    public function __construct(
        GroupRepositoryInterface $customerGroupRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    )
    {
        $this->customerGroupRepository = $customerGroupRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function toOptionArray()
    {
        $options = [];
        $searchResult = $this->customerGroupRepository->getList($this->searchCriteriaBuilder->create());

        /** @var GroupInterface $customerGroup */
        foreach ($searchResult->getItems() as $customerGroup){
            if($customerGroup->getCode() == self::NOT_LOGGED_IN){
                continue;
            }

            $options[$customerGroup->getId()] = $customerGroup->getCode();
        }

        return $options;
    }
}