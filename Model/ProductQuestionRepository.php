<?php
declare(strict_types=1);

namespace Vulpea\Qa\Model;

use Magento\Framework\Api\Search\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Vulpea\Qa\Api\Data\ProductQuestionInterface;
use Vulpea\Qa\Api\Data\ProductQuestionInterfaceFactory;
use Vulpea\Qa\Api\ProductQuestionRepositoryInterface;
use Magento\Framework\Api\Search\SearchResultFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Psr\Log\LoggerInterface;
use Vulpea\Qa\Model\ResourceModel\ProductQuestion\CollectionFactory;

/**
 * Class ProductQuestionRepository
 * @package Vulpea\Qa\Model
 */
class ProductQuestionRepository implements ProductQuestionRepositoryInterface
{
    const ERROR_MESSAGE_LOAD = 'Could not load product question';
    const ERROR_MESSAGE_SAVE = 'Could not save product question';
    const ERROR_MESSAGE_DELETE = 'Could not delete product question';


    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;
    /**
     * @var SearchResultFactory
     */
    private $searchResultFactory;
    /**
     * @var ResourceModel\ProductQuestion
     */
    private $resourceModel;
    /**
     * @var ProductQuestionInterfaceFactory
     */
    private $questionFactory;

    public function __construct(
        LoggerInterface $logger,
        CollectionFactory $collectionFactory,
        SearchResultFactory $searchResultFactory,
        CollectionProcessorInterface $collectionProcessor,
        \Vulpea\Qa\Model\ResourceModel\ProductQuestion $resourceModel,
        ProductQuestionInterfaceFactory $questionFactory
    )
    {
        $this->logger = $logger;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->resourceModel = $resourceModel;
        $this->questionFactory = $questionFactory;
    }

    /**
     * @param ProductQuestionInterface $question
     * @return ProductQuestionInterface
     * @throws CouldNotSaveException
     */
    public function save(ProductQuestionInterface $question): ProductQuestionInterface
    {
        try{
            $this->resourceModel->save($question);
        }catch (\Exception $exception){
            $this->logger->debug($exception->getMessage());
        }

        if(is_null($question->getId())){
            $this->logger->debug(self::ERROR_MESSAGE_SAVE, [self::class]);
            throw new CouldNotSaveException(__(self::ERROR_MESSAGE_SAVE));
        }

        return $question;
    }

    /**
     * @param int $id
     * @return ProductQuestionInterface
     * @throws NoSuchEntityException
     */
    public function getById(int $id): ProductQuestionInterface
    {
        /** @var ProductQuestionInterface $question */
        $question = $this->questionFactory->create();
        $this->resourceModel->load($question, $id);

        if(is_null($question->getId())){
            $this->logger->debug(self::ERROR_MESSAGE_LOAD);
            throw new NoSuchEntityException(__(self::ERROR_MESSAGE_LOAD));
        }

        return $question;
    }

    /**
     * @param ProductQuestionInterface $question
     * @return ProductQuestionInterface
     * @throws CouldNotDeleteException
     */
    public function delete(ProductQuestionInterface $question): ProductQuestionInterface
    {
        try{
            $this->resourceModel->delete($question);
        }catch (\Exception $exception){
            $this->logger->debug($exception->getMessage());
            throw new CouldNotDeleteException(__(self::ERROR_MESSAGE_DELETE));
        }

        return $question;
    }

    /**
     * @param int $id
     * @return ProductQuestionInterface
     * @throws CouldNotDeleteException
     */
    public function deleteById(int $id): ProductQuestionInterface
    {
        try{
            $question = $this->getById($id);
        }catch (\Exception $exception){
            $this->logger->debug($exception->getMessage());
            throw new CouldNotDeleteException(__(self::ERROR_MESSAGE_DELETE));
        }

        return $this->delete($question);
    }


    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Magento\Framework\Api\Search\SearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $searchResult = $this->searchResultFactory->create();

        /** @var \Vulpea\Qa\Model\ResourceModel\ProductQuestion\Collection $collection */
        $collection = $this->collectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);
        $searchResult->setTotalCount($collection->getSize());
        $searchResult->setItems($collection->getItems());
        $searchResult->setSearchCriteria($searchCriteria);

        return $searchResult;
    }
}