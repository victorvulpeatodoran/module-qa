<?php
declare(strict_types=1);

namespace Vulpea\Qa\Model\ResourceModel\ProductQuestion;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Vulpea\Qa\Model\ProductQuestion;

/**
 * Class Collection
 * @package Vulpea\Qa\Model\ResourceModel\ProductQuestion
 * @author Victor Todoran <victor.todoran@evozon.com>
 */
class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(
            ProductQuestion::class,
            \Vulpea\Qa\Model\ResourceModel\ProductQuestion::class
        );
        parent::_construct();
    }
}