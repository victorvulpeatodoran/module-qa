<?php
declare(strict_types=1);

namespace Vulpea\Qa\Model\ResourceModel\ProductAnswer;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Vulpea\Qa\Model\ProductAnswer;

/**
 * Class Collection
 * @package Vulpea\Qa\Model\ResourceModel\ProductAnswer
 * @author Victor Todoran <victor.todoran@evozon.com>
 */
class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(
            ProductAnswer::class,
            \Vulpea\Qa\Model\ResourceModel\ProductAnswer::class
        );
        parent::_construct();
    }
}