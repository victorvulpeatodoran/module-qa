<?php
declare(strict_types=1);

namespace Vulpea\Qa\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Vulpea\Qa\Api\Data\ProductAnswerInterface;
use Vulpea\Qa\Api\Data\ProductQuestionInterface;
use Vulpea\Qa\Model\ProductQuestion as ProductQuestionModel;
use Vulpea\Qa\Model\ProfanityFilter;
use Vulpea\Qa\Model\Validator\QuestionValidator;
use Zend_Validate_Interface;

/**
 * Class ProductQuestion
 * @package Vulpea\Qa\Model\ResourceModel
 * @author Victor Todoran <victor.todoran@evozon.com>
 */
class ProductQuestion extends AbstractDb
{
    /**
     * @var ProfanityFilter
     */
    private $profanityFilter;

    /**
     * @var QuestionValidator
     */
    private $questionValidator;

    public function __construct(
        QuestionValidator $questionValidator,
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        ProfanityFilter $profanityFilter,
        ?string $connectionName = null
    )
    {
        $this->questionValidator = $questionValidator;
        $this->profanityFilter = $profanityFilter;
        parent::__construct($context, $connectionName);
    }

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ProductQuestionModel::ENTITY_TABLE, ProductQuestionInterface::ID);
    }

    /**
     * Filter profanities
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return AbstractDb
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
        /** @var \Vulpea\Qa\Model\ProductQuestion $object */
        $filteredQuestion = $this->profanityFilter->execute($object->getQuestion());
        $object->setData(ProductQuestionInterface::QUESTION, $filteredQuestion);
        return parent::_beforeSave($object);
    }

    /**
     *  Returns the id of the customer who asked the question.
     *  We need it to prevent customers from answering their own questions
     *
     * @param int $questionId
     * @return int|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getQuestionCustomerId(int $questionId): ?int
    {
        $sql = $this->getConnection()->select()
            ->from(['main_table' => $this->getMainTable()], [ProductQuestionInterface::CUSTOMER_ID])
            ->where(ProductQuestionInterface::ID . ' = ?', $questionId)
            ->limit(1);
        $result = $this->getConnection()->fetchOne($sql);

        if(!$result || !is_numeric($result)){
            return null;
        }

        return (int) $result;
    }

    /**
     * Returns ProductQuestionInterface validator. Used in ResourceModel::save().
     *
     * @return null|Zend_Validate_Interface
     */
    public function getValidationRulesBeforeSave()
    {
        return $this->questionValidator;
    }

    /**
     * @param string $startDate
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getProductIdsForQuestionFrom(string $startDate): array
    {
        $connection = $this->getConnection();
        $sql = $connection->select()
            ->from(['main_table' => $this->getMainTable()], ['product_id'])
            ->where('created_at >= ?', $startDate);

        $result = $connection->fetchCol($sql);
        return $result;
    }
}