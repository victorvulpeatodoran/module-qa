<?php
declare(strict_types=1);

namespace Vulpea\Qa\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Vulpea\Qa\Api\Data\ProductAnswerInterface;
use Vulpea\Qa\Model\ProductAnswer as ProductAnswerModel;
use Vulpea\Qa\Model\ProfanityFilter;
use Vulpea\Qa\Model\Validator\AnswerValidator;
use Zend_Validate_Interface;

/**
 * Class ProductAnswer
 * @package Vulpea\Qa\Model\ResourceModel
 * @author Victor Todoran <victor.todoran@evozon.com>
 */
class ProductAnswer extends AbstractDb
{
    /**
     * @var ProfanityFilter
     */
    private $profanityFilter;
    /**
     * @var AnswerValidator
     */
    private $answerValidator;

    public function __construct(
        AnswerValidator $answerValidator,
        ProfanityFilter $profanityFilter,
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        ?string $connectionName = null
    )
    {
        $this->answerValidator = $answerValidator;
        $this->profanityFilter = $profanityFilter;
        parent::__construct($context, $connectionName);
    }

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ProductAnswerModel::ENTITY_TABLE, ProductAnswerModel::ID);
    }

    /**
     * @param int|null $questionId
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function hasAnswerByQuestionId(?int $questionId): bool
    {
        $select = $this->getConnection()->select()
            ->from(['main_table' => $this->getMainTable()], [ProductAnswerInterface::ID])
            ->where(ProductAnswerInterface::QUESTION_ID . ' = ?', $questionId)
            ->limit(1);
        $result = $this->getConnection()->fetchOne($select);

        return (bool) $result;
    }

    /**
     * Filter profanities
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return AbstractDb
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
        /** @var \Vulpea\Qa\Model\ProductAnswer $object */
        $filteredAnswer = $this->profanityFilter->execute($object->getAnswer());
        $object->setData(ProductAnswerInterface::ANSWER, $filteredAnswer);
        return parent::_beforeSave($object);
    }


    /**
     * Returns ProductAnswerInterface validator. Used in ResourceModel::save().
     *
     * @return null|Zend_Validate_Interface
     */
    public function getValidationRulesBeforeSave()
    {
        return $this->answerValidator;
    }
}