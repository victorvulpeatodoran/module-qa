<?php
declare(strict_types=1);

namespace Vulpea\Qa\Model;

use Magento\Framework\Model\AbstractModel;
use Vulpea\Qa\Api\Data\ProductAnswerInterface;

/**
 * Class ProductAnswer
 * @package Vulpea\Qa\Model
 * @author Victor Todoran <victor.todoran@evozon.com>
 */
class ProductAnswer extends AbstractModel implements ProductAnswerInterface
{
    const ENTITY_TABLE = 'product_answer';

    public function _construct()
    {
        $this->_init(\Vulpea\Qa\Model\ResourceModel\ProductAnswer::class);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        if(!$this->hasData(self::ID)){
            return null;
        }

        return (int) $this->_getData(self::ID);
    }

    /**
     * @return null|string
     */
    public function getAnswer(): ?string
    {
        return $this->_getData(self::ANSWER);
    }

    /**
     * @return int|null
     */
    public function getQuestionId(): ?int
    {
        if(!$this->hasData(self::CUSTOMER_ID)){
            return null;
        }

        return (int) $this->_getData(self::QUESTION_ID);
    }

    /**
     * @return int|null
     */
    public function getCustomerId(): ?int
    {
        if(!$this->hasData(self::CUSTOMER_ID)){
            return null;
        }

        return (int) $this->_getData(self::CUSTOMER_ID);
    }

    /**
     * @return null|string
     */
    public function getCreatedAt(): ?string
    {
        return $this->_getData(self::CREATED_AT);
    }

    /**
     * @return null|string
     */
    public function getUpdatedAt(): ?string
    {
        return $this->_getData(self::UPDATED_AT);
    }
}