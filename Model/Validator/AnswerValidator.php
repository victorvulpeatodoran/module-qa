<?php
declare(strict_types=1);

namespace Vulpea\Qa\Model\Validator;

use Vulpea\Qa\Api\Data\ProductAnswerInterface;
use Vulpea\Qa\Model\ProductAnswer;
use Vulpea\Qa\Model\ResourceModel\ProductQuestion;
use Zend_Validate_Exception;
use Zend_Validate_Interface;

/**
 *
 * Class AnswerValidator
 * @package Vulpea\Qa\Model\Validator
 * @author Victor Todoran <victor.todoran@evozon.com>
 */
class AnswerValidator implements Zend_Validate_Interface
{
    const EXCEPTION_CAN_NOT_VALIDATE = "Can not validate object. Object is not instance of ProductAnswerInterface.";

    const ERROR_MISSING_ANSWER = 'Can not add an empty answer';
    const ERROR_INVALID_QUESTION_ID = 'Invalid question id';
    const ERROR_INVALID_CUSTOMER_ID = 'Invalid customer id';
    const ERROR_CAN_NOT_ANSWER_OWN_QUESTION = 'A customer can not answer her own question';

    /**
     * @var string[]
     */
    private $errors = [];

    /**
     * @var ProductQuestion
     */
    private $questionResource;

    public function __construct(
        ProductQuestion $questionResource
    )
    {
        $this->questionResource = $questionResource;
    }

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  mixed $answer
     * @return boolean
     * @throws Zend_Validate_Exception If validation of $value is impossible
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function isValid($answer)
    {
        // Reset error messages from previous validation
        $this->errors = [];

        if(!$answer instanceof ProductAnswerInterface){
            throw new Zend_Validate_Exception(self::EXCEPTION_CAN_NOT_VALIDATE);
        }

        if(is_null($answer->getAnswer()) || !trim($answer->getAnswer())){
            $this->errors[] = self::ERROR_MISSING_ANSWER;
        }

        if(is_null($answer->getQuestionId()) || !is_numeric($answer->getQuestionId())){
            $this->errors[] = self::ERROR_INVALID_QUESTION_ID;
        }

        if(is_null($answer->getCustomerId()) || !is_numeric($answer->getCustomerId())) {
            $this->errors[] = self::ERROR_INVALID_CUSTOMER_ID;
        }

        $questionCustomerId = $this->getQuestionCustomerId($answer->getQuestionId());

        if($answer->getCustomerId() == $questionCustomerId){
            $this->errors[] = self::ERROR_CAN_NOT_ANSWER_OWN_QUESTION;
        }

        return empty($this->errors);
    }

    /**
     * Returns an array of messages that explain why the most recent isValid()
     * call returned false. The array keys are validation failure message identifiers,
     * and the array values are the corresponding human-readable message strings.
     *
     * If isValid() was never called or if the most recent isValid() call
     * returned true, then this method returns an empty array.
     *
     * @return array
     */
    public function getMessages()
    {
        return $this->errors;
    }

    /**
     * @param int $questionId
     * @return int|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getQuestionCustomerId(int $questionId): ?int
    {
        return $this->questionResource->getQuestionCustomerId($questionId);
    }
}