<?php
declare(strict_types=1);

namespace Vulpea\Qa\Model\Validator;

use Vulpea\Qa\Api\Data\ProductQuestionInterface;
use Zend_Validate_Exception;
use Zend_Validate_Interface;

/**
 * Class QuestionValidator
 * @package Vulpea\Qa\Model\Validator
 * @author Victor Todoran <victor.todoran@evozon.com>
 */
class QuestionValidator implements Zend_Validate_Interface
{
    const EXCEPTION_CAN_NOT_VALIDATE = "Can not validate object. Object is not instance of ProductQuestionInterface.";

    const ERROR_PRODUCT_ID = "Product Id must be a numeric value";
    const ERROR_MISSING_NICKNAME = "A question MUST have an associated nickname";
    const ERROR_EMPTY_QUESTION = "A question can not be empty";

    /**
     * @var array
     */
    private $errors = [];


    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  mixed $question
     * @return boolean
     * @throws Zend_Validate_Exception If validation of $value is impossible
     */
    public function isValid($question)
    {
        // Reset error messages from previous validation
        $this->errors = [];

        if(!$question instanceof ProductQuestionInterface){
            throw new Zend_Validate_Exception(self::EXCEPTION_CAN_NOT_VALIDATE);
        }

        $errors = [];
        if(is_null($question->getProductId()) || !is_numeric($question->getProductId())){
            $this->errors[] = self::ERROR_PRODUCT_ID;
        }

        if(is_null($question->getNickname()) || !trim($question->getNickname())){
            $this->errors[] = self::ERROR_MISSING_NICKNAME;
        }

        if(is_null($question->getQuestion()) || !trim($question->getQuestion())){
            $this->errors[] = self::ERROR_EMPTY_QUESTION;
        }

        return empty($this->errors);
    }

    /**
     * Returns an array of messages that explain why the most recent isValid()
     * call returned false. The array keys are validation failure message identifiers,
     * and the array values are the corresponding human-readable message strings.
     *
     * If isValid() was never called or if the most recent isValid() call
     * returned true, then this method returns an empty array.
     *
     * @return array
     */
    public function getMessages()
    {
        return $this->errors;
    }
}