<?php
declare(strict_types=1);

namespace Vulpea\Qa\Model;

use Magento\Framework\Model\AbstractModel;
use Vulpea\Qa\Api\Data\ProductQuestionInterface;

/**
 * Class ProductQuestion
 * @package Vulpea\Qa\Model
 * @author Victor Todoran <victor.todoran@evozon.com>
 */
class ProductQuestion extends AbstractModel implements ProductQuestionInterface
{
    const ENTITY_TABLE = 'product_question';

    public function _construct()
    {
        $this->_init(\Vulpea\Qa\Model\ResourceModel\ProductQuestion::class);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        if(!$this->hasData(self::ID)){
            return null;
        }

        return (int) $this->getData(self::ID);
    }

    /**
     * @return null|string
     */
    public function getNickname(): ?string
    {
        return $this->getData(self::NICKNAME);
    }

    /**
     * @return null|string
     */
    public function getQuestion(): ?string
    {
        return $this->getData(self::QUESTION);
    }

    /**
     * @return int|null
     */
    public function getCustomerId(): ?int
    {
        if(!$this->hasData(self::CUSTOMER_ID)){
            return null;
        }

        return (int) $this->getData(self::CUSTOMER_ID);
    }

    /**
     * @return int|null
     */
    public function getProductId(): ?int
    {
        if(!$this->hasData(self::PRODUCT_ID)){
            return null;
        }

        return (int) $this->getData(self::PRODUCT_ID);
    }

    /**
     * @return null|string
     */
    public function getCreatedAt(): ?string
    {
        return $this->_getData(self::CREATED_AT);
    }

    /**
     * @return null|string
     */
    public function getUpdatedAt(): ?string
    {
        return $this->_getData(self::UPDATED_AT);
    }
}