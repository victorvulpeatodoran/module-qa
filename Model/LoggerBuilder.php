<?php
declare(strict_types=1);

namespace Vulpea\Qa\Model;

use Magento\Framework\Exception\LocalizedException;
use Zend\Log\LoggerInterface;
/**
 * Class LoggerBuilder
 * @package Vulpea\Qa\Model
 * @author Victor Todoran <victor.todoran@evozon.com>
 */
class LoggerBuilder
{
    const MSG_MISSING_FILENAME = 'Filename is not set';
    const MSG_CAN_NOT_CREATE_DIRECTORY = 'Can not create log directory';

    /**
     * @var string|null
     */
    private $fileName;

    /**
     * @param string $fileName
     * @return LoggerBuilder
     */
    public function setFileName(string $fileName): LoggerBuilder
    {
        $this->fileName = $fileName;
        return $this;
    }

    /**
     * @return LoggerInterface
     * @throws LocalizedException
     */
    public function build(): LoggerInterface
    {
        $mkdirResult = true;

        if(is_null($this->fileName)){
            throw new LocalizedException(__(self::MSG_MISSING_FILENAME));
        }

        if(!file_exists(dirname($this->fileName))){
            $mkdirResult = mkdir(dirname($this->fileName));
        }

        if(!$mkdirResult){
            throw new LocalizedException(__(self::MSG_CAN_NOT_CREATE_DIRECTORY));
        }

        $writer = new \Zend\Log\Writer\Stream($this->fileName);
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        $this->reset();

        return $logger;
    }

    /**
     * Reset builder properties
     * @return LoggerBuilder
     */
    public function reset(): LoggerBuilder
    {
        $this->fileName = null;
        return $this;
    }
}