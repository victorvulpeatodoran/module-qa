<?php
declare(strict_types=1);

namespace Vulpea\Qa\Model;

/**
 * Class ProfanityFilter
 * @package Vulpea\Qa\Model
 */
class ProfanityFilter
{
    const FORBIDDEN_WORDS_FILE_ID = 'Vulpea_Qa::etc/profanity_map.csv';

    /**
     * @var \Magento\Framework\Setup\SampleData\FixtureManager
     */
    private $fixtureManager;

    /**
     * @var \Magento\Framework\File\Csv
     */
    private $csvReader;

    /**
     * @var array|null
     */
    private $forbiddenWords;
    /**
     * @var string|null
     */
    private $forbiddenWordsFilePath;


    public function __construct(
        \Magento\Framework\File\Csv $csvReader,
        \Magento\Framework\Setup\SampleData\FixtureManager $fixtureManager
    )
    {
        $this->csvReader = $csvReader;
        $this->fixtureManager = $fixtureManager;
    }

    /**
     * Replaces bad words with **** chars
     *
     * @param $string
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Exception
     */
    public function execute(string $string): string
    {
        $forbiddenWords = $this->getForbiddenWords(
            $this->getForbiddenWordsFilePath(self::FORBIDDEN_WORDS_FILE_ID)
        );

        foreach ($forbiddenWords as $word){
            $string = str_replace($word, str_repeat('*',mb_strlen($word)), $string);
        }

        return $string;
    }

    /**
     * @param string $fileName
     * @return array
     * @throws \Exception
     */
    private function getForbiddenWords(string $fileName): array
    {
        if(is_null($this->forbiddenWords)){
            $forbiddenWords = $this->csvReader->getData($fileName);
            if(empty($forbiddenWords)){
                return $this->forbiddenWords = [];
            }
            $this->forbiddenWords = call_user_func_array('array_merge', $forbiddenWords);
        }

        return $this->forbiddenWords;
    }

    /**
     * @param string $fileId
     * @return null|string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getForbiddenWordsFilePath(string $fileId): string
    {
        if(is_null($this->forbiddenWordsFilePath)){
            $this->forbiddenWordsFilePath =  $this->fixtureManager->getFixture($fileId);
        }

        return $this->forbiddenWordsFilePath;
    }
}