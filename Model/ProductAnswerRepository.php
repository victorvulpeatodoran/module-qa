<?php
declare(strict_types=1);

namespace Vulpea\Qa\Model;

use Magento\Framework\Api\Search\SearchCriteriaInterface;
use Magento\Framework\Api\Search\SearchResultFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Psr\Log\LoggerInterface;
use Vulpea\Qa\Api\Data\ProductAnswerInterface;
use Vulpea\Qa\Api\Data\ProductAnswerInterfaceFactory;
use Vulpea\Qa\Api\ProductAnswerRepositoryInterface;
use Vulpea\Qa\Model\ResourceModel\ProductAnswer\CollectionFactory;


/**
 * Class ProductAnswerRepository
 * @package Vulpea\Qa\Model
 */
class ProductAnswerRepository implements ProductAnswerRepositoryInterface
{
    const ERROR_MESSAGE_LOAD = 'Could not load product answer';
    const ERROR_MESSAGE_SAVE = 'Could not save product answer';
    const ERROR_MESSAGE_DELETE = 'Could not delete product answer';

    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;
    /**
     * @var SearchResultFactory
     */
    private $searchResultFactory;
    /**
     * @var ResourceModel\ProductQuestion
     */
    private $resourceModel;
    /**
     * @var \Vulpea\Qa\Model\ProductAnswerFactory
     */
    private $answerFactory;


    public function __construct(
        LoggerInterface $logger,
        CollectionFactory $collectionFactory,
        SearchResultFactory $searchResultFactory,
        CollectionProcessorInterface $collectionProcessor,
        \Vulpea\Qa\Model\ResourceModel\ProductAnswer $resourceModel,
        ProductAnswerInterfaceFactory $answerFactory
    )
    {
        $this->logger = $logger;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->resourceModel = $resourceModel;
        $this->answerFactory = $answerFactory;
    }

    /**
     * @param ProductAnswerInterface $answer
     * @return ProductAnswerInterface
     * @throws CouldNotSaveException
     */
    public function save(ProductAnswerInterface $answer): ProductAnswerInterface
    {
        try{
            $this->resourceModel->save($answer);
        }catch (\Exception $exception){
            $this->logger->debug($exception->getMessage());
        }

        if(is_null($answer->getId())){
            $this->logger->debug(self::ERROR_MESSAGE_SAVE, [self::class]);
            throw new CouldNotSaveException(__(self::ERROR_MESSAGE_SAVE));
        }

        return $answer;
    }

    /**
     * @param int $id
     * @return ProductAnswerInterface
     * @throws NoSuchEntityException
     */
    public function getById(int $id): ProductAnswerInterface
    {
        /** @var ProductAnswerInterface $answer */
        $answer = $this->answerFactory->create();
        $this->resourceModel->load($answer, $id);

        if(is_null($answer->getId())){
            $this->logger->debug(self::ERROR_MESSAGE_LOAD);
            throw new NoSuchEntityException(__(self::ERROR_MESSAGE_LOAD));
        }

        return $answer;
    }


    /**
     * @param ProductAnswerInterface $answer
     * @return ProductAnswerInterface
     * @throws CouldNotDeleteException
     */
    public function delete(ProductAnswerInterface $answer): ProductAnswerInterface
    {
        try{
            $this->resourceModel->delete($answer);
        }catch (\Exception $exception){
            $this->logger->debug($exception->getMessage());
            throw new CouldNotDeleteException(__(self::ERROR_MESSAGE_DELETE));
        }

        return $answer;
    }

    /**
     * @param int $id
     * @return ProductAnswerInterface
     * @throws CouldNotDeleteException
     */
    public function deleteById(int $id): ProductAnswerInterface
    {
        try{
            $answer = $this->getById($id);
        }catch (\Exception $exception){
            $this->logger->debug($exception->getMessage());
            throw new CouldNotDeleteException(__(self::ERROR_MESSAGE_DELETE));
        }

        return $this->delete($answer);
    }


    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Magento\Framework\Api\Search\SearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $searchResult = $this->searchResultFactory->create();

        /** @var \Vulpea\Qa\Model\ResourceModel\ProductAnswer\Collection $collection */
        $collection = $this->collectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);
        $searchResult->setTotalCount($collection->getSize());
        $searchResult->setItems($collection->getItems());
        $searchResult->setSearchCriteria($searchCriteria);

        return $searchResult;
    }
}