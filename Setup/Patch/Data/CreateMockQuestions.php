<?php
declare(strict_types=1);

namespace Vulpea\Qa\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Vulpea\Qa\Api\Data\ProductQuestionInterface;
use Vulpea\Qa\Model\ProductQuestionFactory;
use Vulpea\Qa\Model\ResourceModel\ProductQuestion as ProductQuestionResource;
use Psr\Log\LoggerInterface;

/**
 * Add Mock Questions to work with.
 * Requires Sample Data with default customer id 1.
 *
 * Class CreateMockQuestions
 * @package Vulpea\Qa\Setup\Patch\Data
 * @author Victor Todoran <victor.todoran@evozon.com>
 */
class CreateMockQuestions implements DataPatchInterface
{
    const FIRST_QUESTION_DATA = [
        ProductQuestionInterface::NICKNAME => 'Vulpea',
        ProductQuestionInterface::QUESTION => 'Is this a good product?',
        ProductQuestionInterface::PRODUCT_ID => 1
    ];

    const SECOND_QUESTION_DATA = [
        ProductQuestionInterface::NICKNAME => 'Vulpea',
        ProductQuestionInterface::QUESTION => 'Are you sure is a good product?',
        ProductQuestionInterface::PRODUCT_ID => 1
    ];

    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var ProductQuestionFactory
     */
    private $productQuestionFactory;
    /**
     * @var ProductQuestionResource
     */
    private $productQuestionResource;


    public function __construct(
        LoggerInterface $logger,
        ProductQuestionFactory $productQuestionFactory,
        ProductQuestionResource $productQuestionResource
    )
    {
        $this->logger = $logger;
        $this->productQuestionFactory = $productQuestionFactory;
        $this->productQuestionResource = $productQuestionResource;
    }
    /**
     * Get array of patches that have to be executed prior to this.
     *
     * example of implementation:
     *
     * [
     *      \Vendor_Name\Module_Name\Setup\Patch\Patch1::class,
     *      \Vendor_Name\Module_Name\Setup\Patch\Patch2::class
     * ]
     *
     * @return string[]
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * Get aliases (previous names) for the patch.
     *
     * @return string[]
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * Run code inside patch
     * If code fails, patch must be reverted, in case when we are speaking about schema - than under revert
     * means run PatchInterface::revert()
     *
     * If we speak about data, under revert means: $transaction->rollback()
     *
     * @return $this
     */
    public function apply()
    {
        try{
            $firstQuestion = $this->productQuestionFactory->create(['data' => self::FIRST_QUESTION_DATA]);
            $firstQuestion->setHasDataChanges(true);
            $this->productQuestionResource->save($firstQuestion);

            $secondQuestion = $this->productQuestionFactory->create(['data' => self::SECOND_QUESTION_DATA]);
            $secondQuestion->setHasDataChanges(true);
            $this->productQuestionResource->save($secondQuestion);
        }catch (\Exception $exception){
            $this->logger->critical($exception->getMessage());
        }

        return $this;
    }
}