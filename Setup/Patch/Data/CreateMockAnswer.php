<?php
declare(strict_types=1);

namespace Vulpea\Qa\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchInterface;
use Psr\Log\LoggerInterface;
use Vulpea\Qa\Api\Data\ProductQuestionInterface;
use Vulpea\Qa\Model\ResourceModel\ProductAnswer as ProductAnswerResource;
use Vulpea\Qa\Model\ProductAnswerFactory;
use Vulpea\Qa\Model\ProductAnswer;
use Vulpea\Qa\Model\ResourceModel\ProductQuestion as ProductQuestionResource;

/**
 * Add Mock Answers to work with.
 * Requires Sample Data with default customer id 1.
 *
 * Class CreateMockAnswer
 * @package Vulpea\Qa\Setup\Patch\Data
 * @author Victor Todoran <victor.todoran@evozon.com>
 */
class CreateMockAnswer implements DataPatchInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var ProductAnswerFactory
     */
    private $productAnswerFactory;
    /**
     * @var ProductAnswerResource
     */
    private $productAnswerResource;
    /**
     * @var ProductQuestionResource
     */
    private $productQuestionResource;

    public function __construct(
        LoggerInterface $logger,
        ProductAnswerFactory $productAnswerFactory,
        ProductAnswerResource $productAnswerResource,
        ProductQuestionResource $productQuestionResource
    )
    {
        $this->logger = $logger;
        $this->productAnswerFactory = $productAnswerFactory;
        $this->productAnswerResource = $productAnswerResource;
        $this->productQuestionResource = $productQuestionResource;
    }

    /**
     * Get array of patches that have to be executed prior to this.
     *
     * example of implementation:
     *
     * [
     *      \Vendor_Name\Module_Name\Setup\Patch\Patch1::class,
     *      \Vendor_Name\Module_Name\Setup\Patch\Patch2::class
     * ]
     *
     * @return string[]
     */
    public static function getDependencies()
    {
        return [
            \Vulpea\Qa\Setup\Patch\Data\CreateMockQuestions::class
        ];
    }

    /**
     * Get aliases (previous names) for the patch.
     *
     * @return string[]
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * Run code inside patch
     * If code fails, patch must be reverted, in case when we are speaking about schema - than under revert
     * means run PatchInterface::revert()
     *
     * If we speak about data, under revert means: $transaction->rollback()
     *
     * @return $this
     */
    public function apply()
    {
        if(!$questionId = $this->getQuestionId()){
            return $this;
        }

        $answerData = [
            ProductAnswer::CUSTOMER_ID => 1,
            ProductAnswer::QUESTION_ID => $questionId,
            ProductAnswer::ANSWER => 'Yes, it is a good product!'
        ];

        try{
            $answer = $this->productAnswerFactory->create(['data' => $answerData]);
            $answer->setHasDataChanges(true);
            $this->productAnswerResource->save($answer);
        }catch (\Exception $exception){
            $this->logger->critical($exception->getMessage());
        }

        return $this;
    }

    /**
     * Get question to assign answer to
     * @return int|null
     */
    private function getQuestionId(): ?int
    {
        $sql = $this->productQuestionResource->getConnection()
            ->select()
            ->from(['main_table' => 'product_question'])
            ->where('product_id = 1')
            ->order('id ASC');
        $row = $this->productQuestionResource->getConnection()->fetchRow($sql);

        if(!$row || !isset($row[ProductQuestionInterface::ID])){
            return null;
        }

        return (int) $row[ProductQuestionInterface::ID];
    }
}