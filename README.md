**Description**
=
These requirements were given, as a final project, to a team of interns after a 3 week Magento 2 **Backend** Development Training.

The team was comprised of 7 people, and they were supposed to work together to complete this task.
The team was given 3-4 days to complete this task. 

I was tasked with doing the assignment in order to asses whether or not the information passed in the the training sessions covers everything needed to complete this task.

I treated this task (as much as I could) as I would treat any other 'real' task. That being said the purpose of this exercise was not to deploy a feature in production, I did not lose track of that either.   

Frontend and style were minimally taken into account.

**FPC must be disabled for this code to work, see at the end of this document why.**

**Screenshots can be found in the Screenshots folder**

**Technical Requirements**
- 

1. A new functionality will be added to Magento: ability to ask questions about a particular product.
2. On the product page, a new tab will be added, that will contain the questions and answers for that specific product. The tab will be called “Q&A”.
3. The question form will contain:
    - a required textarea for the question
    - a required text input for the nickname
    - a submit button.
4. The answer form will contain:
    - a required textarea for the answer
    - a submit button
    - a delete button (see 12.)
5. Under the question form, the list of existing questions and their answers (if there are any) will be displayed. Next to each question, there will be an indication for the type of user that added the question: ‘Guest’ or ‘Verified’.
6. A product can have multiple questions, but a question can have only one answer.
7. The Q&A functionality will be enabled/disabled by a configuration in Magento Admin.
8. Everyone can add a question about a product.
9. Not everyone will be able to add an answer. Customers that belong to a specific customer group will be able to provide answers to any unanswered question (except his own questions).
10. The customer group that enables this ability will be selected in Magento Admin in a configuration. This configuration will be displayed only if the Q&A functionality was enabled and will allow the admin to select a customer group from a dropdown list.
11. A customer can answer questions only if s/he belongs to the configured customer group. Assignation to a customer group is performed manually by the admin.
12. After an answer was provided, the customer who added that answer will be able to delete his/her own answer.
13. The content of the questions will be subject of a “profanity filter”. ‘Bad words’ will be replaced by ‘*****’ chars. 
14. Once the question or answer was submitted, the customer will be notified on the product page about the success/failure of the action.
15. Every day, at 20h00, a report of the products that received new questions during the day will be saved in a log file (a log file for each day).


**FPC & Session Depersonalization**
- 

- Magento implements something called **Session Depersonalization**. 
In a nutshell Magento removes all Sensitive Session data when rendering templates and blocks to ensure sensitive data is not cached. 
This happens when FPC is enabled and when the content (such as the product page) is marked for caching.
There are several ways to go around this one being JS Init Script and use of the CustomerData.js component, the Magento Training did not cover this.

- As mentioned above the product page is cached in FPC, this again means when adding a new question this will not be visible on the page at refresh. JS Init Scripts would be one of the ways to solve this problem. JS Init Scripts were beyond the scope of the training.

- The implementation is this module assumes that FPC is disabled which means two things
    - The (logged in) customer object is available on the session
    - Questions and answers are not cached in FPC
       

