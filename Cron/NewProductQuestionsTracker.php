<?php
declare(strict_types=1);

namespace Vulpea\Qa\Cron;
use Psr\Log\LoggerInterface;
use Vulpea\Qa\Model\ResourceModel\ProductQuestion as QuestionResource;
use Vulpea\Qa\Model\LoggerBuilder as CustomLoggerBuilder;

/**
 * Class NewProductQuestionsTracker
 *
 * @author Victor Todoran <victor.todoran@evozon.com>
 * @package Vulpea\Qa\Cron
 */
class NewProductQuestionsTracker
{
    const ERROR_FAILED_TO_EXECUTE_CRON = 'Failed to fully execute cron!';
    const LOG_FOLDER = '/var/log/product_questions/';
    const LOG_EXTENSION = '.log';
    const STATIC_LOG_MESSAGE = 'There is at least a new question on the product with ID ';

    /**
     * @var LoggerInterface
     */
    private $applicationLogger;

    /**
     * @var QuestionResource
     */
    private $questionResource;

    /**
     * @var CustomLoggerBuilder
     */
    private $customLoggerBuilder;

    /**
     * @var string|null
     */
    private $previousRunDate;

    public function __construct(
        LoggerInterface $logger,
        CustomLoggerBuilder $customLoggerBuilder,
        QuestionResource $questionResource
    )
    {
        $this->applicationLogger = $logger;
        $this->customLoggerBuilder = $customLoggerBuilder;
        $this->questionResource = $questionResource;
    }

    /**
     * Cron entry point
     */
    public function execute(): void
    {
        try{
            $dynamicLogger = $this->customLoggerBuilder
                ->setFileName($this->getLogFileName())
                ->build();

            $previousRunDate = $this->getPreviousRunDate();
            $productIdsWithNewQuestions = $this->questionResource
                ->getProductIdsForQuestionFrom($previousRunDate);
            $productIdsWithNewQuestions = array_unique($productIdsWithNewQuestions);

            foreach ($productIdsWithNewQuestions as $productId){
                $dynamicLogger->info(self::STATIC_LOG_MESSAGE . $productId);
            }

        }catch (\Exception $exception){
            $this->applicationLogger->critical(
                self::ERROR_FAILED_TO_EXECUTE_CRON,
                ['class' => self::class, 'error' => $exception->getMessage()]
            );
        }
    }

    /**
     * @return string
     */
    private function getLogFileName(): string
    {
        return BP . self::LOG_FOLDER . $this->getPreviousRunDate() . self::LOG_EXTENSION;
    }

    /**
     * @return string
     */
    private function getPreviousRunDate(): string
    {
        if(is_null($this->previousRunDate)){
            $today = date('Y-m-d H:i:s');
            $this->previousRunDate = date('Y-m-d H:i:s', strtotime($today .' -1 day'));
        }

        return $this->previousRunDate;
    }
}