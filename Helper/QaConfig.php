<?php
declare(strict_types=1);

namespace Vulpea\Qa\Helper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Vulpea\Qa\Model\Config\Source\AllowedCustomerGroups as CustomerGroups;

/**
 * Class QaConfig
 * @package Vulpea\Qa\Helper
 * @author Victor Todoran <victor.todoran@evozon.com>
 */
class QaConfig implements ArgumentInterface
{
    const PATH_IS_ENABLED = 'catalog/qa/active';
    const PATH_ALLOWED_CUSTOMER_GROUP = 'catalog/qa/allow_customer_group';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var CustomerGroups
     */
    private $customerGroups;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        CustomerGroups $customerGroups
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->customerGroups = $customerGroups;
    }

    /**
     * @return bool
     */
    public function isFeatureEnabled(): bool
    {
        return (bool) $this->scopeConfig->getValue(self::PATH_IS_ENABLED);
    }

    /**
     * @return int|null
     */
    public function getAllowedCustomerGroupId()
    {
        return $this->scopeConfig->getValue(self::PATH_ALLOWED_CUSTOMER_GROUP);
    }

    /**
     * @return null|string
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getAllowedCustomerGroupLabel(): ?string
    {
        $customerGroupCandidates = $this->customerGroups->toOptionArray();
        $allowedCustomerGroupId = $this->getAllowedCustomerGroupId();

        if(isset($customerGroupCandidates[$allowedCustomerGroupId])){
            return $customerGroupCandidates[$allowedCustomerGroupId];
        }

        return null;
    }
}